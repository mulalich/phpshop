<?php

class User {
    public $UserId;
    public $Name;
    public $Surname;
    public $Email;
    public $Password;

    public function __construct($id, $name, $surname, $email, $password)
    {
        $this->UserId = $id;
        $this->Name = $name;
        $this->Surname = $surname;
        $this->Email = $email;
        $this->Password = $password;
    }
}
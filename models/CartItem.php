<?php

class CartItem {
    public $ProductId;
    public $Items;
    public $Size;

    public function __construct($productId, $items, $size)
    {
        $this->ProductId = $productId;
        $this->Items = $items;
        $this->Size = $size;
    }
}
<?php

class Review {

    public $Name;
    public $Description;

    public function __construct($name, $description)
    {
        $this->Name = $name;
        $this->Description = $description;
    }

}
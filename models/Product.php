<?php

class Product {
    public $ProductId;
    public $ProductName;
    public $ProductMiniDescription;
    public $ProductDescription;
    public $ProductPrice;
    public $ImagePath;

    public function __construct($productId, $productName, $productMiniDescription, $productDescription, $productPrice, $productImagePath)
    {
        $this->ProductId = $productId;
        $this->ProductName = $productName;
        $this->ProductMiniDescription = $productMiniDescription;
        $this->ProductDescription = $productDescription;
        $this->ProductPrice = $productPrice;
        $this->ImagePath = $productImagePath;
    }

}
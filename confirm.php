<?php
session_start();
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/controllers/UserController.php");

if(!isset($_SESSION['user'])) { header("Location: index.php"); }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>

<body class="text-center container mt-5" style="max-width: 600px;">
    <h1 class="h3 mb-3 font-weight-normal">Conferma</h1>
    <h1 class="h5 mb-3 font-weight-normal">Il suo ordine è andato a buon fine</h1>
    <a href="index.php" class="btn btn-primary btn-block">Torna alla home</a>
</body>

<?php
$_SESSION['cart'] = array();
?>
</html>

<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/database/dbConnection.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/models/Product.php");

class ProductController {

    static function getAllProducts() {
        $products = array();
        $selectResult = dbConnection::getIstance()->getDb()->query("SELECT * FROM Product");
        if($selectResult->num_rows > 0) {
            while($row = $selectResult->fetch_array(MYSQLI_NUM)) {
                $products[$row[0]] = new Product($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
            }
        }
        return $products;
    }

    static function getProductById($productId) {
        $query = dbConnection::getIstance()->getDb()->prepare("SELECT * FROM Product WHERE ProductId = ?");
        $query->bind_param('i', $productId);
        $query->execute();
        $result = $query->get_result();
        if($result->num_rows > 0) {
            $row = $result->fetch_array(MYSQLI_NUM);
            return new Product($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
        } else {
            return null;
        }
    }

}
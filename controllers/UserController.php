<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/database/dbConnection.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/models/User.php");

class UserController {

    public static function register($name, $surname, $email, $password) {

        $existingUserQuery = dbConnection::getIstance()->getDb()->prepare("SELECT * FROM User WHERE Email =?");
        $existingUserQuery->bind_param('s', $email);
        $existingUserQuery->execute();
        $existingUserQueryResult = $existingUserQuery->get_result();
        if($existingUserQueryResult->num_rows > 0) {
            return null;
        }

        $salt = rand();

        $str = $password.$salt;
        $password_hash = hash('sha256', $str);


        $query = dbConnection::getIstance()->getDb()->prepare("INSERT INTO User(Firstname, Surname, Email, UserPassword, Salt) VALUES (?,?,?,?,?)");
        $query->bind_param('sssss', $name, $surname, $email, $password_hash, $salt);
        $query->execute();

        return new User(0, $name, $surname, $email, $password_hash);
    }

    public static function login($email, $password) {
        $query = dbConnection::getIstance()->getDb()->prepare("SELECT * FROM User WHERE Email = ?");
        $query->bind_param('s', $email);
        $query->execute();
        $result = $query->get_result();

        if($result->num_rows != 1) {
            return null;
        }

        $user_record = $result->fetch_assoc();
        $str = $password.$user_record['Salt'];
        $hash = hash('sha256', $str);
        if ($hash != $user_record['UserPassword']) {
            return null;
        }

        return new User($user_record['UserId'], $user_record['Firstname'], $user_record['Surname'], $user_record['Email'], $user_record['UserPassword']);
    }

}
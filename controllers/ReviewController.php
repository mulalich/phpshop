<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/database/dbConnection.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/models/Review.php");

class ReviewController {

    public static function getReviews($productId) {

        $reviews = array();
        $query = dbConnection::getIstance()->getDb()->prepare("SELECT * FROM Review WHERE FK_ProductId = ?");
        $query->bind_param('i', $productId);
        $query->execute();
        $result = $query->get_result();

        if($result->num_rows > 0) {
            while($row = $result->fetch_array(MYSQLI_NUM)) {
                $reviews[$row[0]] = new Review($row[1], $row[2]);
            }
            return $reviews;
        } else {
            return null;
        }
    }

    public static function addReview($productId, $name, $description) {

        $sanDesc = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $description);

        $sanDesc = str_replace("&", "&amp;", $sanDesc);
        $sanDesc = str_replace('"', "&quot;", $sanDesc);
        $sanDesc = str_replace("'", "&#39;", $sanDesc);
        $sanDesc = str_replace("<", "&lt;", $sanDesc);
        $sanDesc = str_replace(">", "&gt;", $sanDesc);

        $query = dbConnection::getIstance()->getDb()->prepare("INSERT INTO Review(Name, Comment, FK_ProductId) VALUES (?, ?, ?)");
        $query->bind_param('ssi',$name,$sanDesc, $productId);
        $query->execute();
    }



}
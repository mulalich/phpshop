<?php
session_start();
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/controllers/UserController.php");
if(isset($_SESSION['user'])) { header("Location: index.php"); }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Registrazione</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>

<body class="text-center container mt-5" style="max-width: 600px;">

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(isset($_POST['nome']) && isset($_POST['cognome']) && isset($_POST['email']) && isset($_POST['password'])) {
            $response = UserController::register($_POST['nome'], $_POST['cognome'], $_POST['email'], $_POST['password']);
            if($response == null) {
                echo "<div class='alert alert-danger' role='alert'>Un utente si è già registrato con questa email</div>";
            } else {
                echo "<div class='alert alert-success' role='alert'>Il tuo utente è stato registrato con successo</div>";
            }
        }
    }
?>

<form action="#" method="POST">
    <h1 class="h3 mb-3 font-weight-normal">Registrati</h1>
    <label for="inputNome" class="sr-only">Nome</label>
    <input type="text" id="inputNome" class="form-control mb-3" placeholder="Inserisci il tuo nome" name="nome" required>
    <label for="inputCognome" class="sr-only">Password</label>
    <input type="text" id="inputCognome" class="form-control mb-3" placeholder="Inserisci il tuo cognome" name="cognome" required>
    <label for="inputEmail" class="sr-only">Email</label>
    <input type="email" id="inputEmail" class="form-control mb-3" placeholder="Inserisci la tua email" name="email" required>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control mb-3" placeholder="Inserisci una password" name="password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Registrati</button>
    <a href="index.php" class="btn btn-lg btn-secondary btn-block">Torna allo shop</a>
</form>
</body>
</html>

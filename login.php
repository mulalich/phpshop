<?php
session_start();
require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/controllers/UserController.php");

if(isset($_SESSION['user'])) { header("Location: index.php"); }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>

<body class="text-center container mt-5" style="max-width: 600px;">

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['email']) && isset($_POST['password'])) {
        $user = UserController::login($_POST['email'], $_POST['password']);
        if($user == null) {
            echo "<div class='alert alert-danger' role='alert'>Credenziali errate, riprova</div>";
        } else {
            $_SESSION['user'] = serialize($user);
            echo "<div class='alert alert-success' role='alert'>Hai effettuato il login con successo, torna allo shop per cominciare ad effettuare degli ordini</div>";
        }
    }
}
?>

<form action="#" method="POST">
    <h1 class="h3 mb-3 font-weight-normal">Login</h1>
    <input type="email" id="inputEmail" class="form-control mb-3" placeholder="Inserisci la tua email" name="email" required>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control mb-3" placeholder="Inserisci la tua password" name="password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    <a href="index.php" class="btn btn-lg btn-secondary btn-block">Torna allo shop</a>
    <h1 class="h5 mb-3 mt-3 font-weight-normal">Non ancora registrato?</h1>
    <a href="register.php" class="btn btn-lg btn-primary btn-block">Registrati</a>
</form>
</body>
</html>

<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/models/CartItem.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/phpshop/controllers/ProductController.php");
    $total = 0;
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Shoping Cart</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/linearicons-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body class="animsition">
	
	<!-- Header -->
	<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="left-top-bar">
                        I nostri articoli vengono spediti solamente in Svizzera
					</div>

					<div class="right-top-bar flex-w h-full">
                        <?php
                        if(isset($_SESSION['user'])) {
                            echo "<a href='logout.php' class='flex-c-m trans-04 p-lr-25'>Logout</a>";
                        } else {
                            echo "<a href='login.php' class='flex-c-m trans-04 p-lr-25'>Accedi</a>";
                        }
                        ?>
					</div>
				</div>
			</div>

			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">
					
					<!-- Logo desktop -->		
					<a href="index.php" class="logo">
                        <img src="images/dpg_logo.png" alt="IMG-LOGO">
					</a>
				</nav>
			</div>	
		</div>

		<!-- Header Mobile -->
		<div class="wrap-header-mobile">
			<!-- Logo moblie -->		
			<div class="logo-mobile">
				<a href="index.php"><img src="images/dpg_logo.png" alt="IMG-LOGO"></a>
			</div>

			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>


		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="topbar-mobile">
				<li>
					<div class="left-top-bar">
                        I nostri articoli vengono spediti solamente in Svizzera
					</div>
				</li>

				<li>
					<div class="right-top-bar flex-w h-full">
						<a href="#" class="flex-c-m p-lr-10 trans-04">
							Il mio account
						</a>
					</div>
				</li>
			</ul>

		</div>

	</header>

	<!-- Shoping Cart -->
	<form class="bg0 p-t-75 p-b-85">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						<div class="wrap-table-shopping-cart">
							<table class="table-shopping-cart">
                                <?php

                                $products = $_SESSION['cart'];

                                if(count($products) > 0) {
                                    foreach ($products as &$product) {

                                        $productDetail = ProductController::getProductById($product->ProductId);

                                        echo "<tr class=\"table_head\">
									            <th style=\"padding-left: 10px;!important\">Prodotto</th>
                                                <th></th>
                                                <th>Prezzo</th>
                                                <th>Taglia</th>
                                                <th>Quantità</th>
                                                <th>Totale</th>
								            </tr>";

                                        echo "<tr class='table_row'>";
                                        echo "<td style='padding-left: 10px;!important'>";
                                        echo "<div class='how-itemcart1'><img src='images/products/$productDetail->ImagePath'></div>";
                                        echo "</td>";
                                        echo "<td>$productDetail->ProductName</td>";
                                        echo "<td>$productDetail->ProductPrice CHF</td>";
                                        echo "<td>$product->Size</td>";
                                        echo "<td>$product->Items</td>";
                                        $parTot = $product->Items*$productDetail->ProductPrice;
                                        echo "<td>$parTot CHF</td>";
                                        $total = $total + $parTot;
                                    }
                                } else {
                                    echo "<p>Il carrello è vuoto</p>";
                                }

                                ?>
							</table>
						</div>

					</div>
				</div>

				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Totale carrello
						</h4>

						<div class="flex-w flex-t bor12 p-b-13">
							<div class="size-208">
								<span class="stext-110 cl2">
									Totale articoli:
								</span>
							</div>

							<div class="size-209">
								<span class="mtext-110 cl2">
									<?php echo "$total CHF"; ?>
								</span>
							</div>
						</div>

						<div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Spedizione:
								</span>
							</div>

							<div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
                                <div class="size-209">
								<span class="mtext-110 cl2">
									13.00 CHF
								</span>
                                </div>
								
								<div class="p-t-15">
									<span class="stext-112 cl8">
										Indirizzo di spedizione
									</span>

									<div class="bor8 bg0 m-b-12">
										<input type="text" class="stext-111 cl8 plh3 size-111 p-lr-15" name="time" value="Svizzera" readonly>
										<div class="dropDownSelect2"></div>
									</div>

									<div class="bor8 bg0 m-b-12">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="state" placeholder="Via / N° civico">
									</div>

									<div class="bor8 bg0 m-b-22">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="CAP">
									</div>

                                    <div class="bor8 bg0 m-b-22">
                                        <input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="Città">
                                    </div>
										
								</div>
							</div>
						</div>

						<div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Totale:
								</span>
							</div>

							<div class="size-209 p-t-1">
								<span class="mtext-110 cl2">
									<?php $total = $total + 13; echo "$total CHF"; ?>
								</span>
							</div>
						</div>

                        <?php
                        if(isset($_SESSION['user'])) {
                            //echo "<button class='flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer'>Checkout</button>";
                            echo "<div id='paypal-button-container'></div>";
                        } else {
                            echo "<p>Devi essere registrato per poter effettuare degli ordini</p>";
                        }
                        ?>
					</div>
				</div>
			</div>
		</div>
	</form>
		
	
		

	<!-- Footer -->
	<footer class="bg3 p-b-32">
		<div class="container">
			<div class="p-t-40">
				<div class="flex-c-m flex-w p-b-18">
					<span class="m-all-1">
						<img src="images/icons/icon-pay-01.png" alt="ICON-PAY">
					</span>
				</div>

				<p class="stext-107 cl6 txt-center">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

				</p>
			</div>
		</div>
	</footer>


	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

<!-- PAYPAL -->

    <script
        src="https://www.paypal.com/sdk/js?client-id=AfoUH8n1_gKT_iRpia8E3cuy5heYT8_r7xouGgl5z6Ir64SuY7EeN-sGCLP65UtKNK1yPxXAtzFmcR2N&currency=CHF">
    </script>

    <script>
        paypal.Buttons({
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '<?php echo $total?>'
                        }
                    }]
                });
            },
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    window.location.href = "confirm.php";
                    // Call your server to save the transaction
                    return fetch('/paypal-transaction-complete', {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json'
                        },
                        body: JSON.stringify({
                            orderID: data.orderID
                        })
                    });
                });
            }
        }).render('#paypal-button-container');
    </script>

</body>
</html>